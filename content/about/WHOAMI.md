---
title: "WHOAMI"
date: 2020-12-20T22:43:03+01:00
draft: false
---

```
QUE QUIÉN SOY? 😴 | WHO AM I? 😴
```
![<- NO FUNSIONA PACO](../Maiky.jpeg/)


Hey 👋 ! I'm Maiky, and I'm a cybersecurity enthusiast 🌐
(yeah, a "hacker" of those). 
I would like to contribute to the pentesting community. 

I'm also a student, I love physics, especially strings theory
(yeah, like Sheldon Cooper 😑)
I would like to do my bit as best as I can.🎓

-----------------------------------------------------------

Eyy 👋 ! Soy Maiky, y soy un fanático de la ciberseguridad  :globe_with_meridians:
(si, un "hacker" de esos).
Me gustaria aportar mi granito de arena a la comunidad del pentesting.

También soy un estudiante de Bachiller, me gusta la Física
sobretodo la teoría de cuerdas (sí. como Sheldon Cooper 😑)
Me gustaría ayudar a todos mis compañeros.🎓
